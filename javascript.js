let currentPlayer = 'X';
let nextPlayer = 'O';
let playerXSelections = [];
let playerOSelections = [];

const winningCombinations = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7]
];

function checkWinner (playerSelections) {

    for (let i = 0; i < winningCombinations.length; i++) {

        let matches = 0;

        for (let j = 0; j < winningCombinations[i].length; j++) {

            console.log(winningCombinations)
            if (winningCombinations[i].includes(playerSelections[j])) {
                console.log("checking" + playerSelections[j])
                matches = matches + 1

            }
            else {
                break
            }
        }
        console.log("count" + matches)
        if (matches === 3) {
            console.log("three" + matches)
            return true
        }
    }

    return false
}

const handleClick = function (event) {
    const cell = event.target
    console.log(cell.id);
    cell.innerHTML = currentPlayer;
    if (currentPlayer === 'X') {
        playerSelections = playerXSelections;
      
        nextPlayer = 'O';
    } else {
        playerSelections = playerOSelections;
        
        nextPlayer = 'X';
    }
    playerSelections.push(Number(cell.id));
    // Swap players
   
    if (checkWinner(playerSelections)) {
        alert('Player ' + currentPlayer + ' wins!');
        resetGame();
    }
    else {
        console.log("false")
    }
    if (checkDraw()) {
        alert('Draw!');
        resetGame();
    }
    // Swap players
    currentPlayer = nextPlayer;
    
}
const cells = document.querySelectorAll('td');
for (let i = 0; i < cells.length; i++) {
    cells[i].addEventListener('click', handleClick);
}
function checkDraw () {
    return (playerOSelections.length + playerXSelections.length) >= cells.length;
}
function resetGame () {
    playerXSelections = new Array();
    playerOSelections = new Array();
    for (let i = 0; i < cells.length; i++) {
        cells[i].innerHTML = '';
    }
}







